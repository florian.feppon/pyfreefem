Minimal example: Laplace equation 
=================================
    
PyFreeFEM is designed to be intuitive to use. This page explains how    
to write simple comprehensive code for solving the Laplace equation in FreeFEM,  and  then how
to retrieve and to plot the solution in Python.
    

Example script  
--------------

Let us consider the following ``.edp`` script from the   
`examples <https://gitlab.com/florian.feppon/pyfreefem/-/tree/master/pyfreefem/examples>`_  
folder for solving the following Laplace equation on the unit square :math:`\Omega`:
    
.. math::   
    
   \left\{\begin{aligned}   
   -\Delta u & = 1 \text{ in } \Omega,\\    
   u &= 0 \text{ on }\partial \Omega. 
   \end{aligned}\right. 
    



.. literalinclude:: ../../pyfreefem/examples/edp/ex15.edp
   :caption: examples/edp/ex15.edp
   :language: freefem
    
The following python script runs this ``.edp`` file     
and plot the solution with matplotlib:  

.. literalinclude:: ../../pyfreefem/examples/ex15_laplace.py
   :caption: examples/ex15_laplace.py
   :language: python
    

Running     

.. code-block:: console 
    
   python ex15_laplace.py
    
yields the following figure:    

.. figure:: img/u.png
   :width: 400  
   :alt: solution to laplace equation plotted with pymedit
   :align: center

   A `matplotlib` plot of the solution to the Laplace equation computed   
   with FreeFEM

Detailed comments
-----------------
The ``edp/ex15.edp`` script includes     
the following special preprocessing directives for communicating with Python: 
    
* **IMPORT**:
  
  .. code-block:: freefem 
      
     IMPORT "io.edp"
        

  This imports the FreeFEM macro `pyfreefem/edp/io.edp <https://gitlab.com/florian.feppon/pyfreefem/-/tree/master/pyfreefem/edp/io.edp>`_   
  file that define the macros ``exportMesh`` and ``exportArray``.
    
* **Magic variable** ``$N``: 
    
  .. code-block:: freefem

     DEFAULT (N,10)
     mesh Th = square($N,$N);
            
  This variable is      
  automatically assigned when executing the ``.edp`` script from Python with the method    
  :py:meth:`~pyfreefem.FreeFemRunner.execute`. Thanks to the ``DEFAULT`` instruction,     
  ``$N`` will be set to ``10`` if not specified during the execution. The dollar ``$`` symbol is used       
  in the ``.edp`` script
  whenever the magic variable needs to be textually substituted with its value.

The python script ``ex15_laplace.py`` executes ``ex15_laplace.py``  
by setting ``$N`` to ``40``. The output  
variables are contained in the dictionary 
``exports`` which captures the data that have been passed in the ``.edp`` script    
with ``exportMesh`` and ``exportArray``. ``exports['Th']`` is an instance of the ``Mesh``
class implemented in  the `pymedit <https://gitlab.com/florian.feppon/pymedit>`_ module.   


More details about PyFreeFEM special instructions, imports and exports are given in the following   
sections:   

* :doc:`running`
* :doc:`language`     
* :doc:`import_export` 
