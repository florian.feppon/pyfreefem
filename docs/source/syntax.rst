Syntax file for (neo)vim text editor    
====================================    

The package includes an  `edp.vim <syntax/edp.vim>`__ syntax file 
(based on `this
source <https://github.com/FreeFem/FreeFem-parser-vim/blob/master/edp.vim>`__)
which provides coloring for special pyFreeFEM instructions for the
`vim <https://www.vim.org>`__ (or `neovim <https://neovim.io/>`_) text editor.

.. figure:: img/vim.png 
   :align: center   
    
   Syntax coloring for (neo)vim editor   

Installation    
------------

* **vim**   

  Copy ``pyfreefem/syntax/edp.vim`` to ``~/.vim/syntax/edp.vim``. 
    
* **neovim**

  Copy ``pyfreefem/syntax/edp.vim`` to  ``~/.config/nvim/syntax/edp.vim``.   
