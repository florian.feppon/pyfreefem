Importing/Exporting data structures 
=======================================
    
PyFreeFEM automates conversion and exchange of FreeFEM and python data structures.  
The data structures that are supported are listed in the following correspondence table:
    
.. table::  
   :align: center       
   :widths: 80 30 
   :width: 60%

   +---------------------------------------+---------------------------------------------+    
   |   python                              |             FreeFEM                         |    
   +=======================================+=============================================+    
   |  `float`  number                      |      `real` number                          |        
   +---------------------------------------+---------------------------------------------+    
   |  `numpy.array`                        |       `real[int]` array                     |    
   +---------------------------------------+---------------------------------------------+    
   |  `scipy.sparse` matrices              |                                             |
   |  (`csc`, `csr`, `lil` types...)       |       sparse `matrix`                       |
   +---------------------------------------+---------------------------------------------+    
   |  `numpy.ndarray`                      |                                             |
   |  (two dimensional array)              |        `real[int,int]` array                |
   |                                       |                                             |
   +---------------------------------------+---------------------------------------------+
   | `pymedit.Mesh`                        |        `mesh`                               |    
   +---------------------------------------+---------------------------------------------+    
   | `pymedit.Mesh3D`                      |        `mesh3`                              |    
   +---------------------------------------+---------------------------------------------+
    
These data are automatically converted when exported or imported from one language to the other with    
automated reading and writing operations in temporary files.
    
.. admonition:: Mesh data structures 
    
   Meshes are exported from FreeFEM into python with the `Mesh` and `Mesh3D`    
   objects implemented in the `pymedit <https://gitlab.com/florian.feppon/pymedit>`_ library.   
   `pymedit` enables to manipulate triangular/tetrahedral meshes and solution data following    
   the INRIA `mesh` format. 
    

.. _export: 

Exporting FreeFEM data into Python
----------------------------------
    
Exporting FreeFEM data is done from the ``.edp`` script   
by using the macros ``exportVar``, ``exportArray``, ``exportMatrix``,   
``exportMesh`` and ``exportMesh3D`` implemented in 
`pyfreefem/edp/io.edp <https://gitlab.com/florian.feppon/pyfreefem/-/tree/master/pyfreefem/edp/io.edp>`_.
    
These macros still require the special preprocessing instruction
``IMPORT "io.edp"`` at the beginning of the ``.edp`` script. 

.. code-block:: freefem 
   :caption: ex10_io_export.edp 
    
      IMPORT "io.edp"
      
      real var = pi;
      real[int] arr = [1,2,3,4];
      
      mesh Th = square(10,10);
      fespace Fh1(Th,P1);
      
      varf laplace(u,v) = int2d(Th)(u*v+dx(u)*dx(v)+dy(u)*dy(v));
      varf rhs(u,v) = int1d(Th,1)(v);
      
      matrix A = laplace(Fh1,Fh1);
      real[int] f= rhs(0,Fh1);
      
      real[int] u=A^-1*f;
          
      real[int,int] B = [[1,2,3],[4,5,6]]; 
      
      
      dispVar(var);
      exportVar(var);
      export2DArray(B); 

      exportMesh(Th);
      
      exportArray(arr);
      exportArray(f);
      exportArray(u);
      exportMatrix(A);
    
The :meth:`~pyfreefem.FreeFemRunner.execute` method returns a dictionary containing the exported data   
structures.
    

.. code-block:: python  
   :caption: ex10_io_export.py
        
   from pyfreefem import FreeFemRunner
   
   exports = FreeFemRunner("ex10_io_export.edp").execute()
   exports['Th'].plot(title="mesh Th",bcEdgeLabels=True)
   print('var='+str(exports['var']))
   print('A=',exports['A'])
   print('f=',exports['f'])
    
Running this script should show the following output and figure:    
    
.. code-block:: console

   $ python ex10_io_export.py 
   var=3.141592653589793
   A= <Compressed Sparse Column sparse matrix of dtype 'float64'
   	with 761 stored elements and shape (121, 121)>
     Coords	Values
     (0, 0)	1.0016666666666667
     (1, 0)	-0.49958333333333327
     (11, 0)	-0.49958333333333327
     (12, 0)	0.0008333333333333339
     :	:
     (108, 119)	-0.9991666666666669
     (118, 119)	-0.49958333333333343
     (119, 119)	2.0025000000000004
     (120, 119)	-0.49958333333333343
     (108, 120)	0.0008333333333333331
     (109, 120)	-0.49958333333333343
     (119, 120)	-0.49958333333333343
     (120, 120)	1.001666666666667
   B= [[1. 2. 3.]
    [4. 5. 6.]]
   f= [0.05 0.1  0.1  0.1  0.1  0.1  0.1  0.1  0.1  0.1  0.05 0.   0.   0.
    0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.
    0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.
    :    :
    0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.
    0.   0.   0.   0.   0.   0.   0.   0.   0.  ]

.. figure:: img/Th_python.png
   :align: center 

   `pymedit` plot after running `ex10_io_export.py`


Importing python data into FreeFEM  
----------------------------------
    
The data that are listed in the above table can be imported in a ``.edp`` script    
by using the macros ``importVar``, ``importArray``, ``importMatrix``
``importMesh3D`` implemented in 
`pyfreefem/edp/io.edp <https://gitlab.com/florian.feppon/pyfreefem/-/tree/master/pyfreefem/edp/io.edp>`_. 
These macros can be used by inserting the special preprocessing instruction
``IMPORT "io.edp"`` at the beginning of the ``.edp`` script. 
    
  `pyfreefem/edp/io.edp <https://gitlab.com/florian.feppon/pyfreefem/-/tree/master/pyfreefem/edp/io.edp>`_  
  also include the macros ``dispVar``, ``dispArray``, ``dispMatrix`` for conveniently displaying the values of    
  corresponding variables.
    
.. code-block:: freefem 
   :caption: ex11_io_import.edp 
    
   IMPORT "io.edp"
   
   mesh Th=importMesh("Th");
   plot(Th,cmm="Th");
   
   real x = importVar("x");
   dispVar(x);
   
   real[int] arr = importArray("arr"); 
   dispArray(arr);
   
   matrix A = importMatrix("A");
   dispMatrix(A);

   real[int,int] B = import2DArray("B"); 
   disp2DArray(B); 
    
    
In this example, ``Th``, ``x``, ``arr``, ``A`` and ``B`` refer to  
python data structures that must be imported before  
executing the script by using   
the method :py:meth:`~pyfreefem.FreeFemRunner.import_variables` of the     
:py:class:`~pyfreefem.FreeFemRunner` class:
    
.. code-block:: python  
   :caption: ex11_io_import.py
        
   from pyfreefem import FreeFemRunner
   
   import numpy as np
   from pymedit import square
   import scipy.sparse as sp
    
   x = np.pi   
   arr = np.asarray([1,2,3])
   Th = square(10,10)        # a pymedit.Mesh square mesh
   A = sp.diags([1,2,3,4])
   
   runner = FreeFemRunner("ex11_io_import.edp")
       
   runner.import_variables(x, arr, Th, A, B)
   runner.execute(verbosity=0, plot=True)
    
Running this script should show the following output and figure:    
    
.. code-block:: console

   $ python ex11_io_import.py 
   x=3.14159
   arr=3	
        1	  2	  3	
   A=#  HashMatrix Matrix (COO) 0x1534930
   #    n       m        nnz     half     fortran   state  
   4 4 4 0 0 0 0 
            0         0 1
            1         1 2
            2         2 3
            3         3 4

   B=2 3	
      	   1   2   3
      	   4   5   6
        
    
.. figure:: img/Th_freefem.png
   :align: center 

   FreeFem plot after running `ex11_io_import.py`
            
    
Manual imports and exports  
--------------------------
    
Alternatively, you can read and write FreeFEM data structures using     
the following functions from the module :mod:`pyfreefem.io`:    

- :py:meth:`~pyfreefem.io.readFFArray` and :py:meth:`~pyfreefem.io.readFFMatrix`    
  for reading matrices and arrays saved in FreeFEM with `ofstream <https://doc.freefem.org/references/IO.html#ofstream>`_.

- :py:meth:`~pyfreefem.io.writeFFArray` and :py:meth:`~pyfreefem.io.writeFFMatrix` for saving   
  `numpy.array` and `scipy` sparse matrices to files readable by `FreeFEM` with `ifstream <https://doc.freefem.org/references/IO.html#ifstream>`_.
            
    
Floating variables can be directly stored in text files, while `pymedit` mesh objects can be saved into     
``.mesh`` files readable by `FreeFEM` with the method :py:meth:`pymedit.Mesh.save`.
