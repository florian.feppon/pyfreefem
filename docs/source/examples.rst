Examples and test suite
=======================

Examples that illustrate the features of `pyFreeFem`    
are available in the folder `pyfreefem/examples <https://gitlab.com/florian.feppon/pyfreefem/-/tree/master/pyfreefem/examples>`_.   
They can be executed as python modules from the command line with the `-m` option, e.g.:
    

.. code:: bash

   python -m pyfreefem.examples.ex15_laplace
    
    
These examples form a test suite that can be run from the ``pyfreefem`` root folder:  

.. code:: bash

   pytest -vv

List of available examples   
--------------------------

.. dir:: pyfreefem/examples

