# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'pyfreefem'
copyright = '2023, Florian Feppon'
author = 'Florian Feppon'
release = '1.1.0'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

import sys  
import os
sys.path.append(os.path.abspath('lib'))
    
    
#extensions = []
extensions = ['sphinx.ext.autodoc',
              'sphinx.ext.autosummary', 
              'sphinxawesome_theme',    
              'sphinx_contrib_dir']

templates_path = ['_templates']
exclude_patterns = []



# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_static_path = ['_static']
html_theme = "sphinxawesome_theme"
html_extra_path = ["google7bdbeab653ef60e9.html"]
    
add_module_names = False
