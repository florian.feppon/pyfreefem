PyFreeFEM API   
=============   

PyFreeFEM is based on two main classes, ``pyfreefem.Preprocessor`` and
``pyfreefem.FreeFemRunner`` which respectively parse enhanced ``.edp``
scripts and execute them with FreeFEM. In addition, the package provides
a module ``pyfreefem.io`` that includes functions allowing to read and write
FreeFEM arrays or sparse matrix files in python, to display debugging messages   
and to execute shell commands.

API   
---   
   
* **Module** :class:`pyfreefem.FreeFemRunner`  
* **Module** :mod:`pyfreefem.preprocessor`
* **Module** :mod:`pyfreefem.io`
