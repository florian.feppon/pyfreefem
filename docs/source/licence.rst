Licence 
-------
    
``pyFreeFEM`` is a free software distributed under the terms of     
the GNU General Public Licence `GPL3 <https://www.gnu.org/licenses/gpl-3.0.html>`_.
