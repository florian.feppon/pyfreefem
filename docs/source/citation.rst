Citation    
========

Please cite the following reference when using this package:

    FEPPON, Florian. *Shape and topology optimization of multiphysics
    systems.* 2019. Université Paris-Saclay. Thèse préparée à l’École
    polytechnique.
    
.. code-block:: bibtex  

    @phdthesis{feppon2020,
       author = {Feppon, Florian},
       title =  {Shape and topology optimization of multiphysics systems},
       school = {Th\`{e}se de doctorat de l'Universit'{e} Paris-Saclay pr'{e}par'{e}e
          \`a l''{E}cole polytechnique},
       year = {2019}
    }
