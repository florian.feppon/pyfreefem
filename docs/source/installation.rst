Installation
============

You can install PyFreeFEM with pip or by cloning the sources from the gitlab repository. 
    

Installation Requirements   
-------------------------
    
For installing PyFreeFEM, a python executable (version >=3.6) and a recent version of     
`FreeFEM <https://freefem.org/>`_ (>=4.0) need to be installed and available from the command line.    

If you don't install PyFreeFEM with ``pip``, you will also need to manually install the 
following python packages:
    

- `numpy <https://numpy.org/>`_ (>=1.12.1)
- `scipy <https://scipy.org/>`_ (>=0.19.1) 
- `colored <https://pypi.org/project/colored/>`_ (>=1.3.93)    
- `pymedit <https://gitlab.com/florian.feppon/pymedit>`_ (>=1.2)


Installation with pip
---------------------
    
This is the easiest way to get the latest stable version and to install python dependencies.

.. code:: bash

   pip install pyfreefem
    
    
Installation from the gitlab repository
---------------------------------------

Clone the repository using the command line prompt:

.. code:: console

   git clone https://gitlab.com/florian.feppon/pyfreefem.git

Then add the ``pyfreefem`` folder to your ``$PYTHONPATH``:  
    
.. code-block:: bash  
   :caption: .bashrc    
    
   export PYTHONPATH="$PYTHONPATH:/path/to/pyfreefem"

or install the module locally with ``pip``:

.. code:: bash

   pip install -e /path/to/pyfreefem

where ``/path/to/pyfreefem`` is the directory where pyFreeFEM has
been cloned.
    


